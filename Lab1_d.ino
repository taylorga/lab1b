const int L_EYE = A3;
const int R_EYE = A4;

void setup() {
  Serial.begin(9600);
  pinMode(L_EYE, INPUT);
  pinMode(R_EYE, INPUT);

}

void loop() {
  bool all_clear_L = digitalRead(L_EYE);
  bool all_clear_R = digitalRead(R_EYE);
  
  if( !all_clear_L ){
    Serial.println("I see you left!");
 }else{
  Serial.println("No");
 }
 delay( 1000 );

if( !all_clear_R ){
    Serial.println("I see you right!");
 }else{
  Serial.println("No");
 }
 delay( 1000 );
}
