const int LEFT_NEG = 4;
const int LEFT_POS = 5;
const int RIGHT_POS = 6;
const int RIGHT_NEG = 7;
const int L_EYE = A4;
const int R_EYE = A3;
const int PIXY = D2; //this must be the pixy cam pin(s)

void setup() {
  Serial.begin(9600);
  
  pinMode(LEFT_NEG, OUTPUT);
  pinMode(LEFT_POS, OUTPUT);
  pinMode(RIGHT_NEG, OUTPUT);
  pinMode(RIGHT_POS, OUTPUT);
  pinMode(L_EYE, INPUT);
  pinMode(R_EYE, INPUT);
  pinMode(PIXY, INPUT);

  attachInterrupt( digitalPinToInterrupt(PIXY), stop_isr, RISING); // could also try HIGH as stop condition
}

  bool all_clear_L = digitalRead(L_EYE);
  bool all_clear_R = digitalRead(R_EYE);
  
void loop() {
  all_clear_L = digitalRead(L_EYE);
  all_clear_R = digitalRead(R_EYE);
  if ( all_clear_L && all_clear_R){
    forward();
  }
  else if (!all_clear_L && !all_clear_R){
    drive_slow();
  }
  else if( !all_clear_L ){
    turn_left();
  }
  else if (!all_clear_R){
    turn_right();
  }
}

void forward(){
  analogWrite(RIGHT_POS, 170);
  digitalWrite(RIGHT_NEG, LOW);
  analogWrite(LEFT_POS, 150);
  digitalWrite(LEFT_NEG, LOW);
}

void turn_right(){
  analogWrite(RIGHT_POS, 64);
  digitalWrite(RIGHT_NEG, LOW);
  analogWrite(LEFT_POS, 210);
  digitalWrite(LEFT_NEG, LOW);
}

void turn_left(){
  analogWrite(RIGHT_POS, 220);
  digitalWrite(RIGHT_NEG, LOW);
  analogWrite(LEFT_POS, 64);
  digitalWrite(LEFT_NEG, LOW);
}

void stop_driving(){
  digitalWrite(RIGHT_POS, LOW);
  digitalWrite(RIGHT_NEG, LOW);
  digitalWrite(LEFT_POS, LOW);
  digitalWrite(LEFT_NEG, LOW);
}

void drive_slow(){
  analogWrite(RIGHT_POS, 80);
  digitalWrite(RIGHT_NEG, LOW);
  analogWrite(LEFT_POS, 60);
  digitalWrite(LEFT_NEG, LOW);
}

void stop_isr(){
  stop_driving();
}
