import processing.serial.*;
Serial myPort;
String inString;


void setup()
{
  printArray(Serial.list());
  //String portName = Serial.list()[1]; //change to match your port
  myPort = new Serial(this, "COM5", 9600);
  myPort.write("+++");
  delay(1100);
  myPort.write("ATID 3310, CH C, CN");
  delay(1100);
  myPort.bufferUntil(10);
}

void draw() {
}


void keyPressed() {
  myPort.write( key );
  myPort.write('\n');
}

///////////////////////////////
void serialEvent(Serial p) {
  inString = p.readString();
  println(inString);
}
