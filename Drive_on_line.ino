const int LEFT_NEG = 3;
const int LEFT_POS = 5;
const int RIGHT_NEG = 6;
const int RIGHT_POS = 7;
const int L_EYE = A4;
const int R_EYE = A3;

void setup() {
  Serial.begin(9600);
  
  pinMode(LEFT_NEG, OUTPUT);
  pinMode(LEFT_POS, OUTPUT);
  pinMode(RIGHT_NEG, OUTPUT);
  pinMode(RIGHT_POS, OUTPUT);
  pinMode(L_EYE, INPUT);
  pinMode(R_EYE, INPUT);
}

void loop() {
  bool all_clear_L = digitalRead(L_EYE);
  bool all_clear_R = digitalRead(R_EYE);

  if ( all_clear_L && all_clear_R){
    drive_R();
    drive_L();
  }
  else if( !all_clear_L ){
    speed_R();
    slow_L();
  }
  else if (!all_clear_R){
    speed_L();
    slow_R();
  }
}

void drive_R(){
  analogWrite(RIGHT_NEG, 170);
  digitalWrite(RIGHT_POS, LOW);
}

void drive_L(){
  analogWrite(LEFT_NEG, 150);
  digitalWrite(LEFT_POS, LOW);
}

void stop_R(){
  digitalWrite(RIGHT_NEG, LOW);
  digitalWrite(RIGHT_POS, LOW);
}

void stop_L(){
  digitalWrite(LEFT_NEG, LOW);
  digitalWrite(LEFT_POS, LOW);  
}

void slow_R(){
  analogWrite(RIGHT_NEG, 80);
  digitalWrite(RIGHT_POS, LOW);
}

void slow_L(){
  analogWrite(LEFT_NEG, 70);
  digitalWrite(LEFT_POS, LOW);  
}

void speed_R(){
  analogWrite(RIGHT_NEG, 200);
  digitalWrite(RIGHT_POS, LOW);
}

void speed_L(){
  analogWrite(LEFT_NEG, 200);
  digitalWrite(LEFT_POS, LOW);  
}
