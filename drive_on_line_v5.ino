//pins
const int LEFT_NEG = 4;
const int LEFT_POS = 5;
const int RIGHT_POS = 6;
const int RIGHT_NEG = 7;
const int L_EYE = A4;
const int R_EYE = A3;
const int US_TRIG = 9;
const int US_ECHO = 8;

double distance = 0;
int duration = 0;
long time_since;
long time_1 = 0;
long time_2 = 0;
char theChar;

const int STOPPING_DISTANCE = 15;

void setup() {
  Serial.begin(9600);

  pinMode(LEFT_NEG, OUTPUT);
  pinMode(LEFT_POS, OUTPUT);
  pinMode(RIGHT_NEG, OUTPUT);
  pinMode(RIGHT_POS, OUTPUT);
  pinMode(L_EYE, INPUT);
  pinMode(R_EYE, INPUT);
  pinMode(US_ECHO, INPUT);
  pinMode(US_TRIG, OUTPUT);

  delay(1000);//  Guard                     //sets up our XBee
  Serial.print("+++");                      //sets up our XBee
  delay(1000);                              //sets up our XBee
  Serial.println("ATID  3310, CH  C,  CN"); //sets up our XBee
  delay(  1000  );
  while (  Serial.read() != -1);
}

bool all_clear_L = digitalRead(L_EYE);
bool all_clear_R = digitalRead(R_EYE);

void loop() {
  if (Serial.available() > 0) {
    theChar = Serial.read();
    Serial.print("I saw: ");
    Serial.println( theChar );

    if (theChar == 'g') {
      run_buggy();
    }
    else if (theChar == 's') {
      stop_driving();
    }
    else Serial.println("Error, enter valid command");

  }

}

void run_buggy() {
  //Drive on line
  all_clear_L = digitalRead(L_EYE);
  all_clear_R = digitalRead(R_EYE);
  if ( all_clear_L && all_clear_R) {
    forward();
  }
  else if (!all_clear_L && !all_clear_R) {
    drive_slow();
  }
  else if ( !all_clear_L ) {
    turn_left();
  }
  else if (!all_clear_R) {
    turn_right();
  }

  //Ultrasonics
  time_1 = millis();
  time_since = time_1 - time_2;

  digitalWrite(US_TRIG, HIGH);
  delayMicroseconds(10);
  digitalWrite(US_TRIG, LOW);

  time_2 = millis();

  duration = pulseIn(US_ECHO, HIGH);
  distance = duration / 58;
  Serial.println(distance);

  while (distance <= STOPPING_DISTANCE) {
    stop_driving();
    digitalWrite(US_TRIG, HIGH);
    delayMicroseconds(10);
    digitalWrite(US_TRIG, LOW);
    delayMicroseconds(8);
    duration = pulseIn(US_ECHO, HIGH);
    distance = duration / 58;
    Serial.println(distance);
  }
}

void forward() {
  analogWrite(RIGHT_POS, 170);
  digitalWrite(RIGHT_NEG, LOW);
  analogWrite(LEFT_POS, 150);
  digitalWrite(LEFT_NEG, LOW);
}

void turn_right() {
  analogWrite(RIGHT_POS, 64);
  digitalWrite(RIGHT_NEG, LOW);
  analogWrite(LEFT_POS, 210);
  digitalWrite(LEFT_NEG, LOW);
}

void turn_left() {
  analogWrite(RIGHT_POS, 220);
  digitalWrite(RIGHT_NEG, LOW);
  analogWrite(LEFT_POS, 64);
  digitalWrite(LEFT_NEG, LOW);
}

void stop_driving() {
  digitalWrite(RIGHT_POS, LOW);
  digitalWrite(RIGHT_NEG, LOW);
  digitalWrite(LEFT_POS, LOW);
  digitalWrite(LEFT_NEG, LOW);
}

void drive_slow() {
  analogWrite(RIGHT_POS, 80);
  digitalWrite(RIGHT_NEG, LOW);
  analogWrite(LEFT_POS, 60);
  digitalWrite(LEFT_NEG, LOW);
}
